#This is our capstone project git practice
#Please complete the function that you are assigned, test it using the built-in tests, or make your own
#After you are done, commit and push it to your git branch and send it to me to review

#reverse string
def revString(str):
    #YOUR CODE HERE - Erik

#take out spaces in string
def noSpaces(str):
    #YOUR CODE HERE - Peter B

#duplicate string (stringstring)
def duplicateString(str):
    #YOUR CODE HERE - Tommy

#replace spaces with x
def spacesToX(str):
    #YOUR CODE HERE - Peter C

#only return every other letter
def everyOther(str):
    #YOUR CODE HERE - John

#return a list of all the words in a string
def makeList(str):
    #YOUR CODE HERE - Brooks

testString = "This is my string to test"
print ("revString passed!" if revString(testString) == "tset ot gnirts ym si sihT" else "revString failed!")
print ("noSpaces passed!" if noSpaces(testString) == "Thisismystringtotest" else "noSpaces failed!")
print ("duplicateString passed!" if duplicateString(testString) == "This is my string to testThis is my string to test"
       else "duplicateString failed!")
print ("spacesToX passed!" if spacesToX(testString) == "Thisxisxmyxstringxtoxtest" else "spacesToX failed!")
print ("everyOther passed!" if everyOther(testString) == "Ti sm tigt et" else "everyOther failed!")
print ("makeList passed!" if makeList(testString) == ["This", "is", "my", "string", "to", "test"]
       else "makeList failed!")


